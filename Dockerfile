FROM ruby:2.7.2-alpine

WORKDIR /app
RUN apk add --no-cache build-base mysql-client mysql-dev tzdata
COPY Gemfile ./
COPY Gemfile.lock ./
RUN bundle install -j16
COPY . ./