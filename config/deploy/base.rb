# frozen_string_literal: true
set :ecs_tasks, [
  {
    name: "#{fetch(:application)}-#{fetch(:env)}",
    container_definitions: [
      {
        name: "app",
        image: "#{fetch(:docker_registry_host_with_port)}/#{fetch(:application)}:#{fetch(:sha1)}",
        working_directory: '/app',
        command: %w(bundle exec rails s -b 0.0.0.0 -p 3000),
        memory_reservation: 256,
        port_mappings: [{ container_port: 3000, host_port: 0, protocol: "tcp" }],
        essential: true,
        environment: [
          { name: 'RAILS_ENV', value: fetch(:env) },
          { name: 'RAILS_SERVE_STATIC_FILES', value: '1' },
          { name: 'DATABASE_URL', value: fetch(:database_url) },
          { name: 'RAILS_MASTER_KEY', value: fetch(:rails_master_key) },
        ],
        volumes_from: [],
        log_configuration: {
          log_driver: "awslogs",
          options: {
            'awslogs-group' => "/aws/ecs/#{fetch(:application)}/#{fetch(:env)}/app",
            'awslogs-region' => fetch(:ecs_region),
          },
        },
      },
    ],
  },
]

set :ecs_services, [
  {
    name: "#{fetch(:application)}-#{fetch(:env)}",
    load_balancers: [
      {
        target_group_arn: fetch(:target_group_arn),
        container_port: 3000,
        container_name: 'app',
      },
    ],
    desired_count: 1,
    deployment_configuration: { maximum_percent: 200, minimum_healthy_percent: 50 },
  },
]
