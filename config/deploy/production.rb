# frozen_string_literal: true
set :env, 'production'
set :database_url, ENV.fetch('PRODUCTION_DATABASE_URL')
set :target_group_arn, ENV.fetch('PRODUCTION_TARGET_GROUP_ARN')

require_relative 'base'
