# frozen_string_literal: true
set :env, 'staging'
set :database_url, ENV.fetch('STAGING_DATABASE_URL')
set :target_group_arn, ENV.fetch('STAGING_TARGET_GROUP_ARN')

require_relative 'base'
