# frozen_string_literal: true
# config valid for current version and patch releases of Capistrano
lock "~> 3.14.1"

set :application, ENV.fetch('ECS_APPLICATION_NAME')
set :sha1, ENV.fetch('CI_COMMIT_SHA', 'latest')
set :ecs_default_cluster, ENV.fetch('ECS_DEFAULT_CLUSTER')
set :ecs_region, ENV.fetch('AWS_DEFAULT_REGION') # optional, if nil, use environment variable
set :ecs_service_role, "" # default: ecsServiceRole
set :ecs_deploy_wait_timeout, 300 # default: 300
set :docker_registry_host_with_port, ENV.fetch('DOCKER_REGISTROY_HOST_WITH_PORT')
set :rails_master_key, ENV.fetch('RAILS_MASTER_KEY')
